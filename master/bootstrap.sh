#/usr/bin/env bash

# TODO
# yum update -y

# NTP
yum install -y ntp
systemctl enable ntpd --now

# Reconfigure network
modprobe overlay
modprobe br_netfilter
cat > /etc/sysctl.d/99-kubernetes-cri.conf <<EOF
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward  = 1
EOF
sysctl --system
iptables -P FORWARD ACCEPT

# Install go
yum install -y wget
echo Downloading Go...
wget -nv -O go.tar.gz https://dl.google.com/go/go1.13.1.linux-amd64.tar.gz
tar -xzf go.tar.gz
mv go /usr/local
export GOROOT=/usr/local/go
export PATH=$GOROOT/bin:$PATH

# Install CNI plugins
yum install -y git
git clone https://github.com/containernetworking/plugins cni-plugins
cd cni-plugins
git checkout v0.8.2
./build_linux.sh
mkdir -p /opt/cni/bin
cp bin/* /opt/cni/bin
cd ..

cat << EOF > /etc/yum.repos.d/crio.repo
[cri-o]
name=CRI-O
baseurl=https://cbs.centos.org/repos/paas7-crio-115-candidate/x86_64/os
enabled=1
gpgcheck=0
EOF
yum install -y cri-o cri-tools
mkdir -p  /etc/systemd/system/crio.service.d
cat <<EOF > /etc/systemd/system/crio.service.d/0-grpc.conf
[Service]
Environment='GRPC_GO_REQUIRE_HANDSHAKE=off'
EOF
mkdir -p /usr/share/containers/oci/hooks.d
systemctl enable crio
systemctl start crio

# Install kubernetes
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
swapoff -a
sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
mkdir -p  /etc/systemd/system/kubelet.service.d
cat <<EOF > /etc/systemd/system/kubelet.service.d/0-cri.conf
[Unit]
Wants=crio.service
[Service]
Environment='GRPC_GO_REQUIRE_HANDSHAKE=off'
EOF
cat <<EOF > /etc/sysconfig/kubelet
KUBELET_EXTRA_ARGS=\
--runtime-request-timeout=10m \
--cgroup-driver=systemd \
--image-service-endpoint=unix:///var/run/crio/crio.sock \
--container-runtime-endpoint=unix:///var/run/crio/crio.sock
EOF
systemctl daemon-reload
systemctl enable kubelet
systemctl start kubelet

# kubelet requires swap off
# keep swap off after reboot
# sed -i '/ExecStart=/a Environment="KUBELET_EXTRA_ARGS=--cgroup-driver=cgroupfs"' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
# sed -i '0,/ExecStart=/s//Environment="KUBELET_EXTRA_ARGS=--cgroup-driver=cgroupfs"\\n&/' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
# Get the IP address that VirtualBox has given this VM
#echo This VM has IP address $IPADDR
# Writing the IP address to a file in the shared folder
#echo $IPADDR > /vagrant/ip-address.txt
# Set up Kubernetes
kubeadm config images pull
kubeadm init --cri-socket=unix:///var/run/crio/crio.sock
# Set up admin creds for the vagrant user
#echo Copying credentials to /home/vagrant...
#sudo --user=vagrant mkdir -p /home/vagrant/.kube
#cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
#chown $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/config
